
import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerData from "./DrawerContent"
// import Home from "../Screens/Home"
import BottomTab from './BottomTab';
const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator
        screenOptions={{headerShown:false}}
        drawerContent={DrawerData}
           >
            <Drawer.Screen
                name="Home"
                component={BottomTab}
                options={{ drawerLabel: 'open drawer' }}
            />
        </Drawer.Navigator>
    );
};

export default DrawerNavigator;