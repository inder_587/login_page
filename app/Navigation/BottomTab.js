import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet } from 'react-native';
import HomeListing from "../Screens/Home"
import Login from "../Screens/Login/index"
import Profile from"../Screens/Profile"
import Icon from 'react-native-vector-icons/FontAwesome'
import Reels from '../Screens/Reels';
import Favorites from '../Screens/Favorites';



const Tab = createBottomTabNavigator();
const BottomTab = () => {
   
    return (
        <Tab.Navigator tabBarOptions={{ activeTintColor: "black" }} screenOptions={{headerShown:false}}>
            <Tab.Screen

                name="Home"
                component={HomeListing}

                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='navicon'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
            />
                <Tab.Screen
                name="Search"
                component={HomeListing}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='search'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
                />
                <Tab.Screen
                name="Reels"
                component={Reels}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='camera-retro'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
            />
        
        <Tab.Screen
                name="Favorites"
                component={Favorites}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='heart'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
            />
        <Tab.Screen
                name="Profile"
                component={Profile}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='user'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
            />
        

        </Tab.Navigator>
    );
};

export default BottomTab;
const styles = StyleSheet.create({

    tabImg2: {
        alignSelf: 'center',
        tintColor: 'grey',
        height: 25,
        width: 30,
    },
    selectedTabImg2: {
        alignSelf: 'center',
        tintColor: 'black',
        height: 25,
        width: 30,
    },

});