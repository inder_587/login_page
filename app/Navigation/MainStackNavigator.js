
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../Screens/Login';
import Signup from '../Screens/Signup';
import Home from "../Screens/Home";
import BottomTab from './BottomTab';
import Profile from '../Screens/Profile';
import SideDrawer from './DrawerNavigator';
// import Favorites from '../Screens/Favorites';
import Favorites from '../Screens/Favorites';
import Reels from '../Screens/Reels';

const Stack = createStackNavigator();
const MainStackNavigator = () => (
  <Stack.Navigator>
  
    
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
    name="Signup"
    component={Signup}
    options={{headerShown: false}}
    />
    
    <Stack.Screen
    name="Home"
    component={Home}
    options={{headerShown: false}}
    />
    <Stack.Screen
      name="BottomTab"
      component={BottomTab}
      options={{headerShown: false}}
    />
     <Stack.Screen
      name="Profile"
      component={Profile}
      options={{headerShown: false}}
    />
     <Stack.Screen
      name="SideDrawer"
      component={SideDrawer}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Favorites"
      component={Favorites}
      options={{headerShown: false}}
    />

<Stack.Screen
      name="Reels"
      component={Reels}
      options={{headerShown: false}}
    />

 
  </Stack.Navigator>
);

export default MainStackNavigator;