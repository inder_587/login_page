import React, { useState } from 'react';
import { Text, View,TextInput,TouchableOpacity ,FlatList,StyleSheet, ScrollView,Image,} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';



const data = [
  { id: '1', title: 'First item', img:require('../../../assets/3.png')},
  { id: '2', title: 'Second item' },
  { id: '3', title: 'Third item' },
  { id: '4', title: 'Fourth item' }
];

const Favorites = ({navigation}) => {
  
  return (
      <View>
     
      <View style={{flexDirection:"row", justifyContent:"space-between",marginLeft:10}}>
      
     <Text style={{fontSize:25}}>Activity </Text>
     <View style={{marginTop:8,marginRight:20}}>
       <TouchableOpacity>
     <Icon name={'instagram'} size={30} color={"black"}/>
     </TouchableOpacity>
     </View>
     
     </View>
     <ScrollView>
 <View style={{justifyContent:"space-evenly",marginTop:20}}>
   <View  style={{flexDirection:'row',justifyContent:"space-evenly",marginLeft:-10}}>
 <Image  source={ require('../../../assets/2.png')} style={{ width: 50, height: 60,borderRadius:50}}/>
 <Text style={{marginTop:20,fontSize:15}}>Aarzoo_Mandkan21 liked your photo</Text>
 <Image  source={ require('../../../assets/5.png')} style={{ width: 50, height: 60 ,borderRadius:80,marginLeft:10}}/>
 
     </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/girl.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Manpreet_saini liked your photo</Text>
 <Image  source={ require('../../../assets/2.png')} style={{ width: 50, height: 60,borderRadius:80 , marginLeft: 20}}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly", marginLeft: 10}}>
    <Image  source={ require('../../../assets/2.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Aarzoo_Mandkan21 liked your photo</Text>
 <Image  source={ require('../../../assets/pic1.png')} style={{ width: 50, height: 60,borderRadius:80 ,marginLeft:20}}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/vibes.png')} style={{ width:50, height: 50, borderRadius:30  }}/>
 <Text style={{marginTop:20,fontSize:15}}>Anchal544 liked your photo</Text>
 <Image  source={ require('../../../assets/pic1.png')} style={{ width: 50, height: 60,borderRadius:80, marginLeft: 10 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/3.png')} style={{ width: 40, height: 60,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>komal_sharma liked your photo</Text>
 <Image  source={ require('../../../assets/4.png')} style={{ width: 50, height: 50,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/4.png')} style={{ width: 40, height: 50,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Noordeep requested to follow you</Text>
<View style={{flexDirection:"row"}}>
  <TouchableOpacity>
 <View style={{flexDirection:"row",marginTop:15}}>
 <Icon name={'check'} size={30} color="blue" /> 
 </View>
 </TouchableOpacity>
 <TouchableOpacity>
 <View style={{marginTop:12}}>
 <Icon name={'cross'} size={35} color="blue" /> 
 </View>
 </TouchableOpacity>
 </View>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/nature.png')} style={{ width: 40, height: 50,borderRadius:40, marginLeft: -10 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Happy_sharma liked you photo</Text>
 <Image  source={ require('../../../assets/3.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/pic1.png')} style={{ width: 40, height: 60,borderRadius:80, marginLeft: -10 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Neha_verma11 liked you photo</Text>
 <Image  source={ require('../../../assets/5.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/2.png')} style={{ width: 40, height: 60,borderRadius:80, marginLeft:-5 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Aarzoo_Mandkan21 liked you photo</Text>
 <Image  source={ require('../../../assets/laptop.png')} style={{ width: 50, height:48,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/2.png')} style={{ width: 40, height: 60,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Aarzoo_Mandkan21 liked you photo</Text>
 <Image  source={ require('../../../assets/3.png')} style={{ width: 50, height: 55,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/4.png')} style={{ width: 40, height: 55,borderRadius:80, marginLeft: -10 }}/>
 <Text style={{marginTop:15,fontSize:15}}>Manpreet_saini liked you photo</Text>
 <Image  source={ require('../../../assets/4.png')} style={{ width: 50, height: 55,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/5.png')} style={{ width: 40, height: 60,borderRadius:80, marginLeft: -5 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Manpreet_saini liked you photo</Text>
 <Image  source={ require('../../../assets/nature2.png')} style={{ width: 50, height: 50,borderRadius:80, marginRight: -5 }}/>
    </View>

    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/6.png')} style={{ width: 40, height: 60,borderRadius:80, marginLeft: -10 }}/>
 <Text style={{marginTop:20,fontSize:15}}>komal_sharma liked you photo</Text>
 <Image  source={ require('../../../assets/3.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/4.png')} style={{ width: 40, height: 60,borderRadius:80 , marginLeft:-10}}/>
 <Text style={{marginTop:20,fontSize:15}}>Jasmeenkaur99 liked you photo</Text>
 <Image  source={ require('../../../assets/4.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/3.png')} style={{ width: 40, height: 60,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Aarzoo_Mandkan21 liked you photo</Text>
 <Image  source={ require('../../../assets/5.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
    </View>
    <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
    <Image  source={ require('../../../assets/5.png')} style={{ width: 40, height: 60,borderRadius:80 }}/>
 <Text style={{marginTop:20,fontSize:15}}>Manpreet_saini liked you photo</Text>
 <Image  source={ require('../../../assets/5.png')} style={{ width: 50, height: 60,borderRadius:80 }}/>
    </View>
     </View>
     </ScrollView>
  
     </View>
   
    );
    
}

export default Favorites;
const styles = StyleSheet.create({
   contain: {
     marginTop: 10,
     
     paddingHorizontal: 10,
     fontSize: 20,
   },
   sectionTitle: {
     fontSize: 60,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 25,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },
 });