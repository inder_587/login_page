import React, { useState } from 'react';
import { Text, View,TextInput,TouchableOpacity ,FlatList,StyleSheet, ScrollView,Image,} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Video from 'react-native-video';

const Reels = ({navigation}) => {
  
  return (
      <View style={{flex:1}}>
     
      <View style={{flexDirection:"row", justifyContent:"space-between",marginLeft:10}}>
      
     <Text style={{fontSize:25}}>Reels </Text>
     <View style={{marginTop:8,marginRight:20}}>
       <TouchableOpacity>
     <Icon name={'camera'} size={30} color={"black"}/>
     </TouchableOpacity>
     </View>
     </View>
     <View>
      <ScrollView> 
       <View style={{height:'100%',width:'100%',alignItems:"center"}}>
     <Video
     
     source={require('../../../assets/sample-mp4-file-1.mp4')}
    style={{ width:200, height: 200,alignSelf:"center" }}
 />
 <View style={{flexDirection:"row",marginTop:-80,marginLeft:-100}}>
 <View>
      
      <TouchableOpacity>
      <Icon name="heart" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="comment" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="share" size={30} color="black" />
      </TouchableOpacity>
     
</View>
</View>

<Video
     
     source={require('../../../assets/sample-mp4-file-1.mp4')}
    style={{ width:200, height: 200,alignSelf:"center" }}
 />
<View style={{flexDirection:"row",marginTop:-80,marginLeft:-100}}>
 <View>
      
      <TouchableOpacity>
      <Icon name="heart" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="comment" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="share" size={30} color="black" />
      </TouchableOpacity>
     
</View>
</View>
<Video
     
     source={require('../../../assets/sample-mp4-file-1.mp4')}
    style={{ width:200, height: 200,alignSelf:"center" }}
 />
 <View style={{flexDirection:"row",marginTop:-80,marginLeft:-100}}>
 <View>
      
      <TouchableOpacity>
      <Icon name="heart" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="comment" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="share" size={30} color="black" />
      </TouchableOpacity>
     
</View>
</View>
 <Video
     
     source={require('../../../assets/sample-mp4-file-1.mp4')}
    style={{ width:200, height: 200,alignSelf:"center" }}
 />
 <View style={{flexDirection:"row",marginTop:-80,marginLeft:-100}}>
 <View>
      
      <TouchableOpacity>
      <Icon name="heart" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="comment" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="share" size={30} color="black" />
      </TouchableOpacity>
     
</View>
</View>
 <Video
     
     source={require('../../../assets/sample-mp4-file-1.mp4')}
    style={{ width:200, height: 200,alignSelf:"center" }}
 />
 <View style={{flexDirection:"row",marginTop:-80,marginLeft:-100}}>
 <View>
      
      <TouchableOpacity>
      <Icon name="heart" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="comment" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="share" size={30} color="black" />
      </TouchableOpacity>
     
</View>
</View>
 <Video
     
     source={require('../../../assets/sample-mp4-file-1.mp4')}
    style={{ width:200, height: 200,alignSelf:"center" }}
 />
 <View style={{flexDirection:"row",marginTop:-80,marginLeft:-100}}>
 <View>
      
      <TouchableOpacity>
      <Icon name="heart" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="comment" size={30} color="black" />
      </TouchableOpacity>
      </View>

      <View style={{marginTop:4,marginLeft:10 }}>
      <TouchableOpacity>
      <Icon name="share" size={30} color="black" />
      </TouchableOpacity>
     
</View>
</View>
 </View>
 

 </ScrollView>

 </View>
     </View>
    );
    
}

const styles = StyleSheet.create({
  contain: {
    marginTop: 10,
    
    paddingHorizontal: 10,
    fontSize: 20,
  },
  sectionTitle: {
    fontSize: 60,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 25,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default Reels; 