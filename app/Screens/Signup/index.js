import React, {useState} from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  
  Alert
} from "react-native";
 
export default function signup({navigation}){
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const loginFun=()=>{
   
    if(email==""){
     alert("PLease fill Email")
     return 
    }
    else if(password==""){
     alert("Please fill password")
     return 
    } 
    else{
   navigation.navigate('Signup')
    }
      
     }
     
  return (
    <View style={styles.container}>
      
      <Text style={{fontWeight:'bold', marginBottom: 20, alignItems: 'center',fontSize: 30}}>Login Form</Text>
      
      <View style={styles.loginView}>
         <TextInput
          style={styles.TextInput}
          placeholder="Email."
          placeholderTextColor="#003f5c"
          onChangeText={(email) => setEmail(email)}
        />
      </View>
        <View style={styles.loginView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password."
          placeholderTextColor="#003f5c"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
 
      <TouchableOpacity>
        <Text style={styles.forgot_button}>Forgot Password?</Text>
      </TouchableOpacity>
 
      <TouchableOpacity style={styles.loginBtn} onPress={()=>loginFun()}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>

      <TouchableOpacity>
        <Text style={styles.signup}>SIGN UP</Text>
      </TouchableOpacity>

    </View>
  );
}
 
const styles = StyleSheet.create({                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
  container: {
    flex: 1,
    backgroundColor: "#cce6ff",
    alignItems: "center",
    justifyContent: "center",
  },
 
  image: {
    marginBottom: 40,
  },
 
  loginView: {
    backgroundColor: "#b3d9ff",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
 
    alignItems: "center",
  },
 
  
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },
 
  forgot_button: {
    height: 30,
    marginBottom: 30,
  },
 
  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#004d99",
   
  },

  signup: {
  marginTop: 20,
  color: '#66b3ff',
  },

});
