import React, { useState } from 'react';
import { Text,View,TextInput,TouchableOpacity,StyleSheet, Image, ScrollView, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Story from 'react-native-story';
const data=[
  {id: '1' ,title: 'Inderjeetkaur123', img: require('../../../assets/nature.png'), avatarImg:require('../../../assets/girl.png')},
  {id: '2' ,title: 'Aarzoo Mandkan21', img: require('../../../assets/2.png'), avatarImg: require('../../../assets/5.png')},
  {id: '3' ,title: 'Manpreet singh', img: require('../../../assets/nature2.png'), avatarImg: require('../../../assets/6.png')},
  {id: '4' ,title: 'Student club', img: require('../../../assets/laptop.png'), avatarImg: require('../../../assets/pic1.png')},
  {id: '5' ,title: 'Trolls officials213', img: require('../../../assets/frnds.png'), avatarImg: require('../../../assets/girl.png')},
  {id: '1' ,title: 'Aanchal009', img: require('../../../assets/nature.png'), avatarImg:require('../../../assets/girl.png')},
  {id: '1' ,title: 'Sam_Verma', img: require('../../../assets/vibes.png'), avatarImg:require('../../../assets/girl.png')},
  {id: '1' ,title: 'Komalsharma118', img: require('../../../assets/city2.png'), avatarImg:require('../../../assets/girl.png')},
  {id: '1' ,title: 'Jasmeen_kaur_saini', img: require('../../../assets/girl2.png'), avatarImg:require('../../../assets/girl.png')},
];
const Home = ({navigation}) => {
  const stories = [
    {
      id: "4",
      source:require('../../../assets/nature.png'),
      user: "your story ",
      avatar: require('../../../assets/nature.png'),
    },
    {
      id: "2",
      source:require('../../../assets/5.png'),
  
      user: "Mustafa",
      avatar: require('../../../assets/frnds.png'),
    },
    {
      id: "4",
      source:require('../../../assets/4.png'),
      user: "inder ",
      avatar: require('../../../assets/girl2.png'),
    },
    {
      id: "4",
      source:require('../../../assets/6.png'),
      user: "aarzoo ",
      avatar: require('../../../assets/6.png'),
    },
    {
      id: "4",
      source:require('../../../assets/pic1.png'),
      user: "jai ",
      avatar: require('../../../assets/pic1.png'),
    },
    {
      id: "4",
      source:require('../../../assets/3.png'),
      user: "neha ",
      avatar: require('../../../assets/nature2.png'),
    },
  ];

    return(
      <View style={{flex:1}}>


        <View style={{flexDirection:'row'}}>
<Text style={{fontSize:25,marginleft: 20, color:'#060c80' ,marginTop:2, color: "black"}}> Instagram 

        </Text> 
        <View style={{marginTop:4,marginLeft:10 }}>
            <TouchableOpacity>
            <Icon name="instagram" size={30} color="black" />
            </TouchableOpacity>
            </View>

            <View style={{marginTop:4,marginLeft:125 }}>
            <TouchableOpacity>
            <Icon name="plus-square-o" size={30} color="black" />
            </TouchableOpacity>
            </View>

            <View style={{marginTop:4,marginLeft:20 }}>
            <TouchableOpacity>
            <Icon name="inbox" size={30} color="black" />
            </TouchableOpacity>
            </View>
            </View>
    
<View style={{marginTop: -40}}>
 <Story
			unPressedBorderColor="#e95950"
			pressedBorderColor="#ebebeb"
			stories={stories}
			footerComponent={
				<TextInput
					placeholder="Send message"
					placeholderTextColor="white"
				/>
    		}
      />
       </View>
       
   
    <ScrollView>
      <FlatList
              data={data}
              keyExtractor={item => item.id}
              renderItem={({ item }) => (
                <View>
                <View style={{marginTop:20,flexDirection:"row"}}>
             <Image  source={ item.avatarImg} style={{ width: 40, height: 40,borderRadius:50}}/>
              <Text style={{marginTop:10}}>{item.title}</Text>
              </View>
              <View>
              <Image  source={item.img} style={{ width: 400, height: 400,marginLeft:10}}/>
              
            </View>
      
      <View style={{flexDirection:"row"}}>
      <View style={{marginTop:4,marginLeft:10 }}>
            <TouchableOpacity>
            <Icon name="heart" size={30} color="black" />
            </TouchableOpacity>
            </View>

            <View style={{marginTop:4,marginLeft:10 }}>
            <TouchableOpacity>
            <Icon name="comment" size={30} color="black" />
            </TouchableOpacity>
            </View>

            <View style={{marginTop:4,marginLeft:10 }}>
            <TouchableOpacity>
            <Icon name="share" size={30} color="black" />
            </TouchableOpacity>
            </View>
      </View>
      

      
      
            </View>
              )}
            />
        
      
     
            </ScrollView>
          
           </View>
         
          );
          
      }
      const styles = StyleSheet.create({
        contain: {
          marginTop: 10,
          
          paddingHorizontal: 10,
          fontSize: 20,
        },
        sectionTitle: {
          fontSize: 60,
          fontWeight: '600',
        },
        sectionDescription: {
          marginTop: 8,
          fontSize: 25,
          fontWeight: '400',
        },
        highlight: {
          fontWeight: '700',
        },
      });
      export default Home;