import React, { useState } from 'react';
import { Text,View,TextInput,TouchableOpacity,StyleSheet, Image, ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Story from 'react-native-story';
const Profile = ({navigation}) => {
  const stories = [
    {
      id: "4",
      source:require('../../../assets/girl.png'),
      user: "Inderjeet kaur ",
      avatar: require('../../../assets/girl.png'),
    },
    
    
  ];

    return(
        <View style={{flex:1}}>

        
                <View style={{flexDirection:'row'}}>
                <View style={{marginTop:10 }}>
                    <TouchableOpacity>
                    <Icon name="lock" size={20} color="black" />
                    </TouchableOpacity>
                    </View>
        <Text style={{fontSize:25,marginleft: 20, color:'#060c80' ,marginTop:2, color: "black"}}> Inderjeetkaur
        
                </Text> 
               
        
                    <View style={{marginTop:4,marginLeft:125 }}>
                    <TouchableOpacity>
                    <Icon name="plus-square-o" size={30} color="black" />
                    </TouchableOpacity>
                    </View>
        
                    <View style={{marginTop:4,marginLeft:20 }}>
                    <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                    <Icon name="bars" size={30} color="black" />
                    </TouchableOpacity>
                    </View>


                    </View>
          <View style={{flexDirection:"row"}}>  
        <View style={{marginTop: -40}}>
         <Story
                    unPressedBorderColor="#e95950"
                    pressedBorderColor="#ebebeb"
                    stories={stories}
                    footerComponent={
                        <TextInput
                            placeholder="Send message"
                            placeholderTextColor="white"
                        />
                    }
              />
               </View>
               <View style={{marginTop:70,marginLeft:20,flexDirection:"row"}}>
               <Text style={{fontSize:16, marginLeft:30}}> Posts</Text>
        <Text style={{fontSize:16,marginLeft:12}}> Followers</Text>
        <Text style={{fontSize:16,marginLeft:12}}> Following</Text>
                    </View>
                    <View style={{flexDirection:"row",marginLeft:-280}}>
         
         <Text style={{marginTop:35,fontSize:25,marginLeft:70}}>10</Text>
         <Text style={{marginTop:35,fontSize:25,marginLeft:45}}>50</Text></View> 
       
         <Text style={{marginTop:35,fontSize:25,marginLeft:55}}>20</Text>
        
      
      
               </View>  
          
              <View style={{ flexDirection:"row", marginBottom: 300}}>

            <View style={{borderWidth: 1, marginRight: 20,paddingLeft:110, paddingRight: 110, marginLeft: 10, borderColor:'#b6bfba',borderRadius: 5}}>
              <TouchableOpacity>
                <Text style={{textAlign: 'center', paddingTop: 3}}>Edit Profile</Text>
                </TouchableOpacity>
                </View>
             

               <View style={{borderWidth: 1, borderColor:'#b6bfba',marginLeft:  -15, paddingRight: 10,  borderRadius: 5}}>
              <TouchableOpacity>
              <Icon name="angle-down" size={30} color="black" />
              </TouchableOpacity>
              </View>
               </View>

               <View style={{ flexDirection: "row", marginTop: -270}}>
                    <View style={{ marginRight: 20,  paddingRight: 110, marginLeft: 10, }}>
                    <Text style={{fontWeight: 'bold'}}>Story Highlight</Text>
                    </View>

                    <View style={{paddingLeft:80}}>
                    <TouchableOpacity>
              <Icon name="angle-down" size={20} color="black" />
              </TouchableOpacity>
                    </View>
               </View>
              <ScrollView>
              <View style={{ marginTop: 30, flexDirection: "row", justifyContent: 'space-evenly'}}>
              <Image  source={ require('../../../assets/nature.png')} style={{ width: 150, height: 150}}/>
              <Image  source={ require('../../../assets/frnds.png')} style={{ width: 150, height: 150}}/>
              <Image  source={ require('../../../assets/laptop.png')} style={{ width: 150, height: 150}}/>
              </View>
            
              <View style={{ marginTop: 20, flexDirection: "row", justifyContent: 'space-evenly'}}>
              <Image  source={ require('../../../assets/city.png')} style={{ width: 150, height: 150}}/>
              <Image  source={ require('../../../assets/nature2.png')} style={{ width: 150, height: 150}}/>
              <Image  source={ require('../../../assets/vibes.png')} style={{ width: 150, height: 150}}/>
              </View>

              <View style={{ marginTop: 20, flexDirection: "row", justifyContent: 'space-evenly'}}>
              <Image  source={ require('../../../assets/city2.png')} style={{ width: 150, height: 150}}/>
              <Image  source={ require('../../../assets/2.png')} style={{ width: 150, height: 150}}/>
              <Image  source={ require('../../../assets/5.png')} style={{ width: 150, height: 150}}/>
              </View>
              </ScrollView>
              </View>
          
            )};
        
        
        export default Profile;