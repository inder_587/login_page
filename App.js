
import React, {useState, useEffect} from 'react';
import {Alert} from 'react-native';
import {
  
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  useTheme,
} from '@react-navigation/native';
import MainStackNavigator from './app/Navigation/MainStackNavigator';
const App = () => {
  return (
    <NavigationContainer>
      <MainStackNavigator />
    </NavigationContainer>
  );
};
export default App;